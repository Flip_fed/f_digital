// fixed svg show
//-----------------------------------------------------------------------------
svg4everybody();
// placeholder
//-----------------------------------------------------------------------------------
$(window).on('load', function() {
	$('#load').fadeOut(500);
	setTimeout(function() {
		$('#preloader').fadeOut(function() {
			$(this).remove();
		});
	}, 600)
});

$(function() {
	$('input[placeholder], textarea[placeholder]').placeholder();
	scrollToDown();
	navigation();
	accordion();
	portfoliodSlider();
	footerMenu();
	animateSection();
	formSubmitAll();
});

$(window).resize(function() {
	footerMenu();
})

function scrollToDown() {
	var $button = $('.js-scroll-to-down');
	$button.on('click', function() {
		$('html, body').stop().animate({
		'scrollTop': $button.parents('#home').next().offset().top
		}, 900);
	});
}

function navigation() {
	var $header = $('.js-header'),
		$navigationButton = $('.js-navigation-button'),
		$navigation = $('.js-navigation'),
			headerClass = 'mod-fixed',
			headerHide = 'hide',
			buttonClass = 'is-active',
			navClass = 'mod-show';
		
	function showMobileMenu() {
		$HTML.addClass(NOSCROLLCLASS);
		$navigationButton.addClass(buttonClass);
		$header.addClass(headerClass);
		$navigation.addClass(navClass).fadeIn();
	}

	function hideMobileMenu() {
		$HTML.removeClass(NOSCROLLCLASS);
		$navigationButton.removeClass(buttonClass);
		$header.removeClass(headerClass);
		$navigation.fadeOut(function() {
			$(this).removeClass(navClass);
		});
	}

	$navigationButton.on('click', function() {
		if (!$(this).hasClass(buttonClass)) {
			showMobileMenu();
		} else {
			hideMobileMenu();
		}
	});

	$navigation.scroll(function(){
		if ($(this).scrollTop() > 10) {
			$header.addClass(headerHide);
		} else {
			$header.removeClass(headerHide);
		}
	});

	$navigation.find('a[href^="#"]').bind('click', function (e) {
	    e.preventDefault();

	    var target = this.hash,
	    	$target = $(target);

    	$('html, body').stop().animate({
			'scrollTop': $target.offset().top
    	}, 900, 'swing', function () {
			// window.location.hash = target;
		});
		if ($navigationButton.hasClass(buttonClass)) {
	    	hideMobileMenu();
	    }
	});

	$(window).resize(function() {
	    $HTML.removeClass(NOSCROLLCLASS);
		if ($(this).width() >= 1200) {
	    	$navigationButton.hide();
	    	$navigation.show();
			$navigationButton.removeClass(buttonClass);
			$navigation.removeClass(navClass);
			$header.removeClass(headerClass);
	    } else {
	    	$navigationButton.show();
	    	$navigation.hide();
	    }
	});
}

function sectionHash() {
	// $('section').each(function() {
	// 	var $this = $(this);
	// 	$(window).scroll(function() {
	// 		let $scrollTop = $(this).scrollTop(),
	// 			$thisOffset = $this.offset().top-100,
	// 			$thisHeight = $this.outerHeight();

	// 		if ($scrollTop > $thisOffset && $scrollTop < $thisOffset+$thisHeight) {
	// 			window.location.hash = $this.attr('id');
	// 		}
	// 	});
	// });
}

function accordion() {
	var accordion = '.js-el-accordion',
		titleButton = '.list-title',
		content ='.wrap-content',
		popUp = '.content',
		activeClass = 'mod-open';

	$(accordion).on('click', titleButton, function() {
		let $this = $(this);

		if ($(window).width() < 1200) {
			if(!$this.hasClass(activeClass)) {
				$this.addClass(activeClass);
				// $this.closest(accordion).find(content).stop().slideUp();
				$this.siblings(content).stop().slideDown();
			} else {
				$this.removeClass(activeClass);
				$this.siblings(content).stop().slideUp();
			}
		} else {
			if(!$this.hasClass(activeClass)) {
				$this.addClass(activeClass);
				$this.closest(accordion).find(popUp).hide();
				$this.siblings(content).children(popUp).fadeIn();
			} else {
				$this.removeClass(activeClass);
				$this.siblings(content).children(popUp).fadeOut();
			}
		}
	});

	$(document).on('click', function(e) {
		if ($(popUp).is(':visible') && $(popUp).css('position') == 'absolute' 
			&& $(popUp).has(e.target).length === 0 && $(accordion).find(titleButton).has(e.target).length == 0) {
				$(popUp).fadeOut();
		}
	});

	$(window).resize(function() {
		if ($(window).width() < 1200) {
			$(accordion).find(content).hide();
			$(accordion).find(popUp).show();
		} else {
			$(accordion).find(popUp).hide();
			$(accordion).find(content).show();
		}
	});
}

function portfoliodSlider() {
	var $sliderWrap = $('.js-slider-wrap'),
		$sliderWindow = $sliderWrap.children('.js-slider-window'),
		$sliderDescriptions = $sliderWrap.children('.js-slider-descriptions');

	$sliderWindow.slick({
		mobileFirst: true,
		infinite: true,
  		dots: false,
  		arrows: true,
  		prevArrow: '<button type="button" class="arrows prev-slide"><svg class="icon icon-arrow-button"><use xlink:href="img/sprite.svg#arrow"></use></svg></button>',
		nextArrow: '<button type="button" class="arrows next-slide"><svg class="icon icon-arrow-button"><use xlink:href="img/sprite.svg#arrow"></use></svg></button>',
		slidesToShow: 1,
		slidesToScroll: 1,
  		cssEase: 'ease-out',
  		swipeToSlide: true,
  		focusOnSelect: true,
		responsive: [
		  	{ breakpoint: 767,
		  		settings: { slidesToShow: 2,}
		  	},
		  	{ breakpoint: 991,
				settings: { slidesToShow: 3,}
			},
			{ breakpoint: 1199,
				settings: { slidesToShow: 4,}
			},
		]
	});

	$sliderWindow.on('setPosition', function () {
		let slideActive = $(this).slick('slickCurrentSlide');
		$sliderDescriptions.children('.items').eq(slideActive).fadeIn().siblings().hide();
	});
}

function footerMenu() {
	var $menu = $('.js-footer-menu'),
		$button = $menu.children('.title'),
			list = 'ul',
			mobileActive = 'js-mobile-button',
			activeClass = 'is-active';

	if ($(window).width() < 1200) {
		$button.addClass(mobileActive);
		$button.siblings(list).hide();
	} else {
		$button.removeClass(mobileActive);
		$button.siblings(list).show();
	}

	$button.on('click', function() {
		if ($(this).hasClass(mobileActive)) {
			$(this).stop().toggleClass(activeClass);
			$(this).siblings(list).stop().slideToggle();
		}
	});	
}

function animateSection() {
	var $sections = $('[data-animate-section]'),
		$items = $sections.find('[data-animate-items]'),
			activeClascc = 'mod-show';

	$items.each(function() {
		var $this = $(this);
	
		$(window).scroll(function() {
			let $scrollTop = $(this).scrollTop(),
				$itemsOffset = $this.offset().top,
				$windowHeight = $(window).height(),
				$thisHeight = $this.outerHeight();

			if($scrollTop >= $itemsOffset-$windowHeight+50
				&& $scrollTop <= $itemsOffset+$thisHeight-50) {
				$this.addClass(activeClascc);
			} else {
				$this.removeClass(activeClascc);
			}
		});
	});
}

function formSubmitAll() {
	var $form = $('form');

	isFormValid($form);

	$form.keypress(function() {
		let $ariaRequired = $(this).find('[aria-required]'),
			$submit = $(this).find('[type="submit"]'),
			validClass = '.valid',
			modValid = 'mod-valid',
			modInvalid = 'mod-invalid';

		if ($ariaRequired.length == $ariaRequired.filter(validClass).length) {
			$submit.removeClass(modInvalid).addClass(modValid);
		} else {
			$submit.removeClass(modValid).addClass(modInvalid);
		}
	});

	$form.submit(function() {
        var th = $(this);
        console.log($(this))
        $.ajax({
			type: "POST",
			url: "php/mail.php",
			data: th.serialize(),
        }).done(function() {     
			setTimeout(function() {
				th.trigger("reset");
				$submit.removeClass(modValid);
			}, 2000);
        });
		return false;
	});
}