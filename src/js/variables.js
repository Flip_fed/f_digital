'use strict';

var BREAKPOINTS = {
  lg: 1200,
  md: 992,
  sm: 768,
  xs: 320
};

/* Elements */
var $HTML = $('html');


/* Classes */
var	NOSCROLLCLASS = 'no-scroll';